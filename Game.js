function Game (file) {
    this.file = file;
    
    var nextMoves = [];
    var prevMoves = [];

    console.log('Game created with file' + this.file.name);

    $('#next, #prev').css('display','block');
    var reader = new FileReader();
    reader.onload = function(theFile) {
        return function(e) {
            console.log('Parsing', e.target.result);
            parseMoves(e.target.result);
        }
    }(this.file);
    reader.readAsText(this.file);

    parseMoves = function (movesStr) {
        var moves = movesStr.split('\n');
        moves.forEach(element => {
            var move = element.split(' ');
            nextMoves.push(move[0], move[1]);
        });
        renderMoves();
    }

    this.makeMove = function () {
        var move = nextMoves.splice(0, 1)[0];
        var begin = move.substr(0, 2);
        var end = move.substr(2, 4);
        prevMoves.push(move);
        $(`#sq${ end }`).html($(`#sq${ begin }`).html());
        $(`#sq${ begin }`).html('');
        if (move === '5131') {
            move = '1141';
        } else if(move === '5171') {
            move = '8161';
        } else if (move === '5878') {
            move = '8868';
        } else if (move === '5838') {
            move = '1848';
        } else {
            move = null;
        }
        if (move) {
            var begin = move.substr(0, 2);
            var end = move.substr(2, 4);
            $(`#sq${ end }`).html($(`#sq${ begin }`).html());
            $(`#sq${ begin }`).html('');
        }
        if(nextMoves.length <= 0 || (nextMoves.length === 1 && nextMoves[0] === undefined)) $('#makeMoveBtn').hide();
        renderMoves();
    }

    function renderMoves () {
        $('#next').html('<center><b>Следующие ходы</b></center>');
        nextMoves.forEach(element => {
            if (element) $('#next').append(`<center>${ element }</center>`);
        });
        $('#prev').html('');
        prevMoves.forEach(element => {
            if (element) $('#prev').prepend(`<center>${ element }</center>`);
        });
        $('#prev').prepend('<center><b>Предыдущие ходы</b></center>');
    }
}