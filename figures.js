$(function() {
    window.figures = {
        PAWN : 'pawn', // Пешка 8шт
        KNIGHT : 'knight', // Конь 2шт.
        BISHOP : 'bishop', // Слон 2шт.
        ROOK : 'rook', // Ладья 2шт.
        QUEEN : 'queen', // Королева 1шт.
        KING : 'king' // Король 1шт.
    };

    window.figuresSource = {
        white : {
            pawn : 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/45/Chess_plt45.svg/50px-Chess_plt45.svg.png',
            knight : 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/70/Chess_nlt45.svg/50px-Chess_nlt45.svg.png',
            bishop : 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b1/Chess_blt45.svg/50px-Chess_blt45.svg.png',
            rook : 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Chess_rlt45.svg/50px-Chess_rlt45.svg.png',
            queen : 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/Chess_qlt45.svg/50px-Chess_qlt45.svg.png',
            king : 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/42/Chess_klt45.svg/50px-Chess_klt45.svg.png'
        },
        black : {
            pawn : 'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c7/Chess_pdt45.svg/50px-Chess_pdt45.svg.png',
            knight : 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/ef/Chess_ndt45.svg/50px-Chess_ndt45.svg.png',
            bishop : 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Chess_bdt45.svg/50px-Chess_bdt45.svg.png',
            rook : 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/Chess_rdt45.svg/50px-Chess_rdt45.svg.png',
            queen : 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/Chess_qdt45.svg/50px-Chess_qdt45.svg.png',
            king : 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Chess_kdt45.svg/50px-Chess_kdt45.svg.png'
        }
    }
    
    $('#gamefileInput').change((event) => {
        var files = event.target.files;
        toggleModal();
        loadFile(files[0]);
        $('#gamefileInput')[0].value = '';
    });

    $('#loadGameBtn, .overlay').click(toggleModal);
    $(document).keyup((event) => {
        // Hide modal on escape press
        if (event.keyCode === 27) $('.modal.open').removeClass('open');
    });

    function toggleModal() {
        $('.modal').toggleClass('open');
    }

    var game = null;
    
    function loadFile(file) {
        $('#makeMoveBtn').unbind('click');
        $('#resetBtn').unbind('click');
        renderField();
        renderFigures();
        game = new Game(file);
        $('#makeMoveBtn').show();
        $('#makeMoveBtn').click(() => {
            game.makeMove();
        });
        $('#resetBtn').show();
        $('#resetBtn').click(() => {
            renderField();
            game = null;
            $('#resetBtn, #makeMoveBtn').hide();
            $('#next').html('<center><b>Следующие ходы</b></center>');
            $('#prev').html('<center><b>Предыдущие ходы</b></center>');
            $('#next, #prev').hide();
        });
    }

    function renderFigures() {
        $('#sq11,#sq81').html(`<img src="${ figuresSource.white.rook }" class="figure" />`);
        $('#sq18, #sq88').html(`<img src="${ figuresSource.black.rook }" class="figure" />`);
        $('#sq21,#sq71').html(`<img src="${ figuresSource.white.knight }" class="figure" />`);
        $('#sq28, #sq78').html(`<img src="${ figuresSource.black.knight }" class="figure" />`);
        $('#sq31,#sq61').html(`<img src="${ figuresSource.white.bishop }" class="figure" />`);
        $('#sq38, #sq68').html(`<img src="${ figuresSource.black.bishop }" class="figure" />`);
        $('#sq12, #sq22, #sq32, #sq42, #sq52, #sq62, #sq72, #sq82').html(`<img src="${ figuresSource.white.pawn }" class="figure" />`);
        $('#sq17, #sq27, #sq37, #sq47, #sq57, #sq67, #sq77, #sq87').html(`<img src="${ figuresSource.black.pawn }" class="figure" />`);
        $('#sq41').html(`<img src="${ figuresSource.white.queen }" class="figure" />`);
        $('#sq48').html(`<img src="${ figuresSource.black.queen }" class="figure" />`);
        $('#sq51').html(`<img src="${ figuresSource.white.king }" class="figure" />`);
        $('#sq58').html(`<img src="${ figuresSource.black.king }" class="figure" />`);
    }

    function renderField() {
        $('.board').html('');
        for(var x = 0; x <= 8; x++) {
            $('.board').append(`<div class="row" id = "row${ x }">`);
            for(var y = 8; y >= 0; y--) {
                if (x === 0) {
                    $(`#row${ x }`).append(`<div class="square letter">${y !== 0 ? y : '&nbsp;'}</div>`);
                } else if (y === 0) {
                    $(`#row${ x }`).append(`<div class="square letter">${ String.fromCharCode(x + 64) }</div>`);
                } else  {
                    $(`#row${ x }`).append(`<div id = "sq${ x }${ y }" class="square ${ (x + y) % 2 === 0 ? 'black' : 'white' }"></div>`);
                }
            }
        }
        $('.board').append('<div><b>Примеры сценариев:</b> <a href="./games/morphi.txt" download>Морфи</a>');
    }

    renderField();

    $('#gamefileInput').on('dragover', (event) => {
        event.preventDefault();
        $('.drop_zone').show();
        $('#gamefileInput').css('opacity','0');
    })

    $('#gamefileInput').on('dragleave', (event) => {
        event.preventDefault();
        $('.drop_zone').hide();
        $('#gamefileInput').css('opacity','1');
    })
});